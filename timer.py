#!/usr/bin/env python3

"""Small timer application to log time of activity."""

from __future__ import print_function


CMD_NAMES = ['on', 'off', 'chrono', 'restop', 'addline', 'plot_totals']


import sys
import os
import os.path
import socket # function gethostname
import signal
import re
import datetime as dt
import argparse

from time import sleep

# datetime string format
DT_FMT = "%Y-%m-%d_%H:%M:%S"
DT_FMT2 = "%Y-%m-%d_%H:%M"
TIME_FMT = "%H:%M:%S"
TIME_FMT2 = "%H:%M"
DURATION_FMT = '{1:02d}h{2:02d}m{3:02d}s'
DURATION_REGEX  = re.compile(          r'(\d\d)h([0-5]\d)m([0-5]\d)s')
TOTALTIME_REGEX = re.compile(r'(\d+)d ([0-2]\d)h([0-5]\d)m([0-5]\d)s')

START = dt.datetime.now()
START_STR = START.strftime(DT_FMT)

HOST = socket.gethostname()
# Format of log file:
# one line per sequence (host, start, end, duration)
# lines for weekly/monthly/yearly totals
#
# Format of stats file:
# line1: starting date
# line2: total time
# line3: weekly average
# line4: on/off

CONFIG_DIR  = os.path.expanduser("~/.config/timer")
STATS_FILE  = os.path.expanduser("~/.config/timer/timer-stats.txt")
LOG_FILE    = os.path.expanduser("~/.config/timer/timer-log.txt")

# Quick conversion between string and booleans
STATES = {'on': True, 'off': False}
ON_OFF = {True: 'on', False: 'off'}


def seconds_to_humantime(sec):
    sec = int(sec)
    days = sec // (24*3600)
    rest = sec % (24*3600)
    hours = rest // 3600
    rest = rest % 3600
    minutes = rest // 60
    seconds = rest % 60
    return [days, hours, minutes, seconds]


def init_dir():
    if not os.path.isdir(CONFIG_DIR):
        os.makedirs(CONFIG_DIR)


def parse_timestr_lastday(timestr):
    """Parse a formatted time string. If no date is given, the time is
    assumed to be past, i.e in the last 24 hours.
    Formats allowed:
        %Y-%m-%d_%H:%M:%S
        %Y-%m-%d_%H:%M
        %H:%M:%S
        %H:%M
    If you give the last two forms, the day will be completed, so that this time
    occured in the *past* 24 hours.
    It means that if you want to give a time for today in the future, for
    example 15:30 when it's only 13:30, you will *have to* use the two first
    formats.
    """
    now = dt.datetime.now()
    try:
        timeval = dt.datetime.strptime(timestr, DT_FMT)
    except ValueError: # Wrong format
        try: 
            timeval = dt.datetime.strptime(timestr, DT_FMT2)
        except ValueError:
            # Now try formats where the day is not given.
            try:
                # With seconds
                timeval = dt.datetime.strptime(timestr, TIME_FMT)
            except ValueError:
                # Without seconds
                timeval = dt.datetime.strptime(timestr, TIME_FMT2)

            timeval = timeval.replace(year=now.year, month=now.month,
                                      day=now.day)
            if timeval > now:
                # if time is given without date, it is assumed to have happened
                # in the past:
                # subtract 1 day (24 hours) if necessary
                timeval = timeval - dt.timedelta(hours=24)
    return timeval


def parse_logline(line):
    """return the current state, and the last line of the log:
        host, time of start [, time of end, duration]"""
    logline = line.rstrip('\r\n').split('\t')
    # Parse first field: host:tag;tag...
    logline[1] = set(logline[1].split(';')) # tags
    logline[2] = dt.datetime.strptime(logline[2], DT_FMT)
    if len(logline) == 5:
        logline[3] = dt.datetime.strptime(logline[3], DT_FMT)
        hours, minutes, seconds = [int(x) for x in \
                                   DURATION_REGEX.match(logline[4]).groups()]
        logline[4] = dt.timedelta(hours=hours, minutes=minutes, seconds=seconds)
        state = False # off
    elif len(logline) == 3:
        state = True # on
    else:
        raise IndexError("Log line incorrectly formatted (expects 5 or 3 fields)")
        
    return state, logline


#def format_logline(logline):
def format_duration(timedelta):
    duration = seconds_to_humantime(timedelta.total_seconds())
    duration[1] += 24*duration[0]
    return DURATION_FMT.format(*duration)


def parse_status():
    """Get last line of the logfile, check the status of the timer, and return
    the parsed last line"""
    with open(LOG_FILE) as lf:
        # Keep only the last line. not sure this is the most efficient.
        for line in reversed(lf.readlines()):
            break
    return parse_logline(line)


def parse_stats():
    # Format of stats file:
    # line1: starting date
    # line2: total time
    # line3: weekly average
    try:
        stats = {}
        with open(STATS_FILE) as sf:
            for line in sf:
                fields = line.rstrip().split('\t')
                stats[fields[0]] = fields[1]
    except IOError as e:
        stats = {'start date': START_STR,
                 'total time': '0d 00h00m00s',
                 'weekly average': '00h00m00s'}
    return stats

def write_stats(stats):
    with open(STATS_FILE, 'w') as sfout:
        sfout.write("start date\t%s\n" % stats['start date'])
        sfout.write("total time\t%s\n" % stats['total time'])
        sfout.write("weekly average\t%s\n" % stats['weekly average'])


def update_log(state, new_time, old_time=None, tags=None, host=HOST):
    on_off = ON_OFF[state]
    new_time_str = new_time.strftime(DT_FMT)
    with open(LOG_FILE, 'a') as log:
        if not state:
            duration_str = format_duration((new_time - old_time))
            log.write("\t%s\t%s\n" % (new_time_str, duration_str))
        else:
            #tag_str = ';'.join(tags) if tags else ''
            tag_str = tags or ''
            log.write("%s\t%s\t%s" % (host, tag_str, new_time_str))


def update_stats(timedelta):
    """Used when timer set to off"""
    #timedelta = new_time - old_time
    #assert timedelta > dt.timedelta(0) # It is allowed to be negative!
    stats = parse_stats()
    total_time = TOTALTIME_REGEX.match(stats['total time']).groups()
    total_time = [int(t) for t in total_time]
    days, hours, minutes, seconds = seconds_to_humantime(timedelta.total_seconds())
    seconds += total_time[3]
    minutes += total_time[2]
    hours   += total_time[1]
    days    += total_time[0]
    minutes += seconds // 60
    hours   += minutes // 60
    days    += hours   // 24
    total_time[3] = seconds % 60
    total_time[2] = minutes % 60
    total_time[1] = hours % 24
    total_time[0] += days
    stats['total time'] = '{0}d {1:02d}h{2:02d}m{3:02d}s'.format(*total_time)
    write_stats(stats)


def check_state(state):
    """Check that you are not trying to set the timer to the state it's
    already in."""
    try:
        old_state, lastline = parse_status()
    except IOError as e:
        old_state = False
        lastline = None

    if old_state is state:
        on_off = ON_OFF[state]
        #raise RuntimeError("The timer is already %s. Nothing done." %on_off)
        print("The timer is already %s. Nothing done." %on_off, file=sys.stderr)
        sys.exit(1)

    return lastline


def update(state, start=START, tags=None, host=HOST):
    """state is a boolean: True = "on", False = "off"."""
    #on_off = ON_OFF[state] 
    #state = STATES[on_off]
    lastline = check_state(state)
    previous_time = lastline[3] if state else lastline[2]
    
    # Check that START is not older than the last time:
    if previous_time > start:
        raise AssertionError("Cannot use a time earlier than the previous time point (%s)." % 
                  previous_time.strftime(DT_FMT))
    
    # the 3d arg will be ignored if state is True ("on")
    update_log(state, start, previous_time, tags, host)

    if not state:
        update_stats((start - previous_time))


### Functions for analysing time log

def load_log():
    logdata = pd.read_table(LOG_FILE, names=['host', 'start', 'end', 'duration'])
    logdata.start = pd.to_datetime(logdata.start, format=DT_FMT)
    logdata.end   = pd.to_datetime(logdata.end,   format=DT_FMT)
    logdata.duration = pd.to_timedelta(logdata.duration)
    return logdata


def get_day(datetime, day_start):
    """return the day of datetime if the time is after the chosen day_start
    E.g: if date is 2016-11-27_04:00 and day_start is 06:00, the date would be
    set to the day before (worked all night!)
    
    datetime
    """
    day = dt.date(datetime.year, datetime.month, datetime.day)
    time = dt.time(datetime.hour, datetime.minute, datetime.second)
    if time < day_start:
        day -= dt.timedelta(hours=24)
    return day


def get_week(datetime, week_start=0):
    """return the date where the week started"""
    # Unused argument: week_start
    weekday = datetime.weekday()
    week = dt.date(datetime.year, datetime.month, datetime.day)
    week -= dt.timedelta(hours=weekday*24)
    return week


def get_month(datetime, month_start):
    # Unused argument: month_start
    return dt.date(datetime.year, datetime.month)


GET_PERIOD = {'day': get_day,
              'week': get_week,
              'month': get_month}


def aggregate_period(logdata, periodname, periodstart=None, resolution='m'):
    """generic function to sum durations over a time period (day, week, month)"""
    # I am not splitting timespans that start one day and end another.
    # If it happens, the starting day is assigned.
    get_period_args = (periodstart,) if periodstart else ()
    print(get_period_args)
    logdata[periodname] = logdata.start.apply(GET_PERIOD[periodname],
                                              args=get_period_args)
    period_data = logdata.groupby(by=[periodname])
    period_totals = period_data.duration.agg(sum).astype('timedelta64[%s]' % resolution)
    #period_totals
    return period_totals
# TODO: also aggregate the std dev of the dayly total


def aggregate_dayly(logdata, day_start_str='06:00', resolution='m'):
    day_start = dt.datetime.strptime(day_start_str, TIME_FMT2)
    day_start = dt.time(day_start.hour, day_start.minute)
    return aggregate_period(logdata, 'day', day_start, resolution)

def aggregate_weekly(logdata, resolution='h'):
    return aggregate_period(logdata, 'week', resolution=resolution)

def aggregate_monthly(logdata, resolution='h'):
    return aggregate_period(logdata, 'month', resolution=resolution)


AGGREGATE_PERIOD = {'day':   aggregate_dayly,
                    'week':  aggregate_weekly,
                    'month': aggregate_monthly}


def plot_totals(periodname='day'):
    """Plot total worked time per day/week/month"""
    global mpl, plt, pd
    import matplotlib as mpl
    mpl.use('TkAgg') # 'Qt4Agg'
    import matplotlib.pyplot as plt
    import pandas as pd

    logdata = load_log()
    period_totals = AGGREGATE_PERIOD[periodname](logdata)
    period_totals.plot.bar()
    plt.show()


### COMMAND-LINE
def on(start_str=None, tags=None, host=HOST):
    """Activate timer."""
    if start_str:
        global START, START_STR
        START = parse_timestr_lastday(start_str)
        START_STR = START.strftime(DT_FMT)
    #tags = set(tags.split(';')) if tags else set()
    update(True, START, tags, host)

def off(start_str=None):
    """Stop timer. Takes no arguments"""
    start = parse_timestr_lastday(start_str) if start_str else START
    update(False, start)


def _chrono_sigint_action(signal, frame):
    sigint_time = dt.datetime.now()
    #update_log(False, sigint_time, START)
    #update_stats((sigint_time - START))
    update(False, sigint_time)
    print("\nEnded at :  %s" % sigint_time.strftime(DT_FMT))
    sys.exit(0)


def chrono(start_str=None, tags=None, host=HOST):
    """Start counting time in the foreground."""
    #tags = set(tags.split(';')) if tags else set()
    #if start_str:
    #    global START, START_STR
    #    START = parse_timestr_lastday(start_str)
    #    START_STR = START.strftime(DT_FMT)
    #lastline = check_state(True)

    ## Check that START is not older than the last stop:
    #if lastline[3] > START:
    #    print("Cannot start before the last end time (%s)." % 
    #          lastline[3].strftime(DT_FMT),
    #          file=sys.stderr)
    #    sys.exit(1)

    #update_log(True, START, None, tags, host)
    on(start_str, tags, host)

    print("Started at: %s (stop with <Ctrl-C>)" % START_STR)
    signal.signal(signal.SIGINT, _chrono_sigint_action)
    while 1:
        duration_str = format_duration((dt.datetime.now() - START))
        print('\r    %s' % duration_str, end='')
        sleep(1)


def subtract(duration):
    """subtract time if you forgot to stop the timer"""
    raise NotImplementedError


def addline(start_str, stop_str, day=None, tags=None, host=HOST):
    if day:
        # apply the same day to everyone
        assert len(start_str.split('_')) == 1 and len(stop_str.split('_')) == 1
        start_str = day + '_' + start_str
        stop_str = day + '_' + stop_str

    on(start_str, tags, host)
    off(stop_str)


def restop(correct_time_str):
    """edit end time if you forgot to stop the timer"""
    lastline = check_state(True)
    wrong_time = lastline[3]
    correct_time = parse_timestr_lastday(correct_time_str)

    # Check that the correct_time is not older than the last start time:
    if lastline[2] > correct_time:
        print("Cannot stop before the previous start time (%s)." % 
              lastline[2].strftime(DT_FMT),
              file=sys.stderr)
        sys.exit(1)

    lastline[1] = ';'.join(lastline[1])
    new_duration = correct_time - lastline[2]
    new_duration_str = format_duration(new_duration)
    lastline[2] = lastline[2].strftime(DT_FMT)
    lastline[3] = correct_time.strftime(DT_FMT)
    lastline[4] = new_duration_str
    # pop the last line, edit it, and append again.
    with open(LOG_FILE) as lf:
        lines = lf.readlines()
        lines.pop()
    lines.append("%s\n" % "\t".join(lastline))
    with open(LOG_FILE, 'w') as lfout:
        lfout.writelines(lines)
    
    update_stats((correct_time - wrong_time))


#class UnknownCommandError(KeyError):
#    pass
#class checkArgs(object):
#    """Decorator for the module functions:
#    if a wrong number of arguments is used, exit the script with help message"""
#    def __init__(self, func, give_command_instance):
#        self.func = func
#        self.give_command = give_command_instance
#
#    def __call__(self, *args, **kwargs):
#        try:
#            self.func(*args, **kwargs)
#        except TypeError:
#            self.give_command.help_command(self.func.__name__)


#def unknown_command(cmd_name):
#    """Display error and quit when the command is invalid."""
#    print(__doc__ + '\n\nERROR: Unknown command %r' % cmd_name,
#          file=sys.stderr)
#    sys.exit(1)
#
#    # should I rather override 'get'/'__getitem__' ?
#def get_command(cmd_name):
#    """return the command if exist, otherwise execute the error."""
#    try:
#        return COMMAND[cmd_name]
#    except KeyError:
#        unknown_command(cmd_name)

#def help_exit(cmd_name=None, error_msg=None):
#    exit = 0
#    msg = __doc__ if not cmd_name else get_command(cmd_name).__doc__ or ''
#    if error_msg:
#        exit = 1
#        msg += '\n\nERROR: ' + error_msg
#    print(msg, file=sys.stderr)
#    sys.exit(exit)
#
#
#def help_command(cmd_name=None):
#    """display the help of a given command, or of the entire script."""
#    help_exit(cmd_name)


#def init_commands(cmd_names):
#    global COMMAND, __doc__
#    COMMAND = {}
#    module_objects = globals()
#    module_objects['help'] = help_command
#    #cmd_names.append('help')
#    for cmd_name in cmd_names:
#        COMMAND[cmd_name] = module_objects[cmd_name]
#    
#    __doc__ = __doc__.format(**{cmd_name: COMMAND[cmd_name].__doc__ for
#                                cmd_name in cmd_names})
#
#
#def run_command(cmd_name, cmd_args):
#    command = get_command(cmd_name)
#    try:
#        command(*cmd_args)
#    except TypeError as err:
#        #print(command, cmd_args)
#        #raise
#        error_msg = ' '.join(err.args)
#        error_msg += '\nWrong number of arguments for command %r' % cmd_name
#        help_exit(cmd_name, error_msg)
#
#def run_commandline(args=sys.argv[1:]):
#    """Execute command with args as passed to the script."""
#    if not args:
#        help_exit(error_msg='Missing command.')
#    else:
#        cmd_name, *cmd_args = args
#    run_command(cmd_name, cmd_args)


#init_commands(CMD_NAMES)
def run_commandline(cmd_names):
    parser = make_parsers(cmd_names)
    args = parser.parse_args()
    dictargs = vars(args)
    #module_objects = globals()
    cmd_name = dictargs.pop('command')
    cmd = dictargs.pop('cmd_func')
    cmd(**dictargs)



def make_parsers(cmd_names):
    prog = os.path.basename(sys.argv[0])
    master_parser = argparse.ArgumentParser(prog=prog, description=__doc__,
                         formatter_class=argparse.RawDescriptionHelpFormatter)
    # parent_parser = arg... add_help=False # (generic arguments)
    module_objects = globals()

    addcmd = master_parser.add_subparsers(dest='command')
    
    for cmd_name in cmd_names:
        cmd = module_objects[cmd_name]
        #commands[cmd_name] = cmd
        #sub_prog = "%s %s" % (prog, cmd_name)
        sub_parser = addcmd.add_parser(cmd_name, description=cmd.__doc__,
                        formatter_class=argparse.RawDescriptionHelpFormatter)
        try:
            # Python 3
            cmd_code = cmd.__code__
            defaultlist = cmd.__defaults__ or []

        except AttributeError:
            # Python 2.7
            cmd_code = cmd.func_code
            # no **kwargs or *args allowed
            assert cmd_code.co_argcount == cmd_code.co_stacksize
            defaultlist = cmd_code.func_defaults

        arglist = cmd_code.co_varnames[:cmd_code.co_argcount]
        start_option = len(arglist) - len(defaultlist)
        defaultlist = [None] * start_option + list(defaultlist)
        argkwargs = [{'default': deflt,
                      'help': '[%(default)s]' * (deflt is not None)}
                            for deflt in defaultlist]

        # If only optional arguments, make the first one positional
        if start_option == 0 and arglist:
            start_option = 1
            argkwargs[0]['nargs'] = '?'
        
        for i, (opt, kwargs) in enumerate(zip(arglist, argkwargs)):
            if i < start_option:
                opts = [opt]
            else:
                # Unless there is a conflicting single-letter option. TODO
                opts = ['-' + opt[0], '--' + opt]
                
            # handle boolean options
            if isinstance(kwargs['default'], bool):
                kwargs['action'] = 'store_' + str(kwargs.pop('default')).lower()

            try:
                sub_parser.add_argument(*opts, **kwargs)
            except argparse.ArgumentError:
                opts[0] = opts[0].upper()
                sub_parser.add_argument(*opts, **kwargs)

        sub_parser.set_defaults(cmd_func=cmd)
            
    #sub_parsers[cmd_name] = sub_parser
    return master_parser



#COMMAND = dict(
#           on     = update_on,
#           off    = update_off,
#           chrono = chrono,
#           restop = restop,
#           help   = help_cmd)
#COMMAND['-h'] = COMMAND['--help'] = COMMAND['help']

#def parse_args():
#    """parse command line arguments
#    """
#    if len(sys.argv) < 2 or sys.argv[1] not in COMMAND:
#        print("Allowed commands: " + ' '.join(COMMAND.keys()), file=sys.stderr)
#        sys.exit(1)
#
#    return sys.argv[1:]

# I finally decide to implement this CLI with argparse


if __name__=='__main__':
    #args = parse_args()
    init_dir()
    run_commandline(CMD_NAMES)

